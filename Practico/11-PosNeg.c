#include <stdio.h>

int main(){
    int dato, positivos=0, negativos=0;

    do{
        printf("Ingrese un dato\n");
        scanf("%d",&dato);

        if (dato>0)
        {
            positivos++;
        }
        else if(dato<0)
        {
           negativos++;
        }        

    }while(dato !=0);

    printf("La cantidad de positivos es: %d \n La cantidad de negativos es: %d",positivos,negativos);
}
