#include <stdio.h>

int main(){

    int numero, inverso = 0, digito;

    printf("Ingrese el numero a invertir\n");
    scanf("%d",&numero);

    while(numero != 0){
        inverso *= 10;
        digito = numero % 10;
        inverso += digito;
        numero = numero/10;
    }
    printf("El numero inverso es: %d", inverso);

}