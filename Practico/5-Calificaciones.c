#include <stdio.h>

int main(){
    char calificacion;
    printf("Ingrese su calificacion de A a F\n");
    scanf("%c",&calificacion);
    
    switch (calificacion)
    {
    case 'A': case 'a':
        printf("Excelente");
        break;
    case 'B': case 'b':
        printf("Buena");
        break;
    case 'C': case 'c':
        printf("Regular");
        break;
    case 'D': case 'd':
        printf("Suficiente");
        break;
    case 'F': case 'f':
        printf("No suficiente");
        break;    
    default:
        printf("Calificacion no valida");
        break;
    }

}