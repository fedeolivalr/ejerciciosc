#include <stdio.h>

int main(){
    int dato;

    do
    {
        printf("Ingrese un valor entre 1 y 9\n");
        scanf("%d",&dato);
    } while (dato < 1 || dato > 9);
    
    for(int i=1;i<=9;i++){
        printf("%d x %d = %d \n",dato,i,(dato*i));
    }
    return 0;
}