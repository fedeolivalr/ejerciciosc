#include <stdio.h>

int main(){
    int decimal, binario=0, multiplicador = 1;
    printf("Ingrese el numero decimal a convertir en binario\n");
    scanf("%d",&decimal);

    while(decimal != 0){
        binario += (decimal % 2)*multiplicador;
        decimal /= 2;
        multiplicador *= 10;    
    }

    printf("El numero binario es: %d",binario);

}