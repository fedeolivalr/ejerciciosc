#include <stdio.h>
#include <math.h>
float potenciasPI(int);
int main(){  

    for(int i=2;i<=10;i++){
        printf("Potencia %d de PI es igual a %f y su raiz cuadrada es %f\n",i,potenciasPI(i),sqrt(potenciasPI(i)));
    }
}

float potenciasPI(int pot){
    float resultado=1;
    for(int i=0; i<pot;i++){        
        resultado = resultado*3.14159265358979323846;
    } 
    return resultado;
}