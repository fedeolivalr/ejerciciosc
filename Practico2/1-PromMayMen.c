#include <stdio.h>
void cargarArray(int array[],int t);
int Mayor(int numeros[], int limite);
int Menor(int numeros[], int limite);
float Promedio(int numeros[], int limite);

int main(){
    int t;
    printf("Ingrese tamanio del Array\n");
    scanf("%d",&t);    
    int numeros[t];
    cargarArray(numeros,t);

    printf("El mayor es %d\n", Mayor(numeros,t));
    printf("El menor es %d\n", Menor(numeros,t));
    printf("El promedio es %f\n", Promedio(numeros,t));

}

void cargarArray(int array[],int t){ 
     for (int i = 0; i < t; i++)
    {
        printf("Ingrese el valor %d\n",i+1);
        scanf("%d",&array[i]);
    } 
}

int Mayor(int numeros[], int limite){
    int mayor = numeros[0];
    for(int i=1; i<limite;i++){
        if(mayor < numeros[i]){
            mayor = numeros[i];
        }
    }
    return mayor;
}

int Menor(int numeros[], int limite){
    int menor = numeros[0];
    for(int i=1; i<limite;i++){
        if(menor > numeros[i]){
            menor = numeros[i];
        }
    }
    return menor;
}

float Promedio(int numeros[], int limite){
    float promedio=0;
    for (int i = 0; i < limite; i++){
       promedio+=numeros[i];
    }
    return promedio/limite;    
}